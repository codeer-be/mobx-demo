import {computed, observable} from "mobx";
import {IProfile} from "../../domain/models/profile";
import {Store} from "../store";
import {profileApi} from "./profile.api";

export class ProfileStore extends Store {
    @observable public profile: IProfile;

    @computed
    public get fullName(): string {
        return this.profile ? `${this.profile.firstName} ${this.profile.lastName}` : '';
    }

    public async fetchProfile(): Promise<void> {
        this.profile = await profileApi.fetchProfile();
    }
}
