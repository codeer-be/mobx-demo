import {inject, observer} from "mobx-react";
import * as React from "react";
import {AppStore} from "../../stores/app/app.store";
import {ProfileStore} from "../../stores/profile/profile.store";

interface IHeaderProps {
    appStore?: AppStore;
    profileStore?: ProfileStore;
}

@inject(({appStore, profileStore}) => ({
    appStore,
    profileStore,
}))
@observer
export class Header extends React.Component<IHeaderProps> {
    public render(): React.ReactNode {
        return (
            <header className="mdl-layout__header">
                <div role="button" className="mdl-layout__drawer-button" onClick={this.openDrawer}>
                    <i className="material-icons"></i>
                </div>
                <div className="mdl-layout__header-row">
                    <span className="mdl-layout-title">MobX Demo</span>
                    <div className="mdl-layout-spacer"/>
                    <span>{this.props.profileStore!.fullName}</span>
                </div>
            </header>
        )
    }

    private openDrawer = (): void => {
        this.props.appStore!.toggleMenu();
    }
}