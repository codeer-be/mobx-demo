import {observable} from "mobx";
import {IProfile} from "../../domain/models/profile";
import {Store} from "../store";
import {usersApi} from "./users.api";

export class UsersStore extends Store {
    @observable public users: IProfile[] = [];
    @observable public isUsersLoading: boolean = false;

    public async fetchUsers(): Promise<void> {
        this.isUsersLoading = true;
        this.users = await usersApi.fetchUsers();
        this.isUsersLoading = false;
    }
}
