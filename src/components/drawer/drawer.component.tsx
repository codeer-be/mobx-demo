import {inject, observer} from "mobx-react";
import * as React from "react";
import {AppStore} from "../../stores/app/app.store";

interface IDrawerProps {
    appStore?: AppStore;
}

@inject(({appStore}) => ({
    appStore,
}))
@observer
export class Drawer extends React.Component<IDrawerProps> {
    public render(): React.ReactNode {
        return (
            <>
                <div className={`mdl-layout__drawer ${this.props.appStore!.navigation.menu.isOpen && 'is-visible'}`}>
                    <span className="mdl-layout-title">MobX Demo</span>
                    <nav className="mdl-navigation">
                        <a className="mdl-navigation__link" href="">Link</a>
                        <a className="mdl-navigation__link" href="">Link</a>
                        <a className="mdl-navigation__link" href="">Link</a>
                        <a className="mdl-navigation__link" href="">Link</a>
                    </nav>
                </div>
                <div className={`mdl-layout__obfuscator ${this.props.appStore!.navigation.menu.isOpen && 'is-visible'}`} onClick={this.onClickObfuscator}/>
            </>
        )
    }

    private onClickObfuscator = (): void => {
        this.props.appStore!.toggleMenu();
    }
}