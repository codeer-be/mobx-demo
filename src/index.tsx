import {Provider} from "mobx-react";
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {App} from './app.component';
import registerServiceWorker from './registerServiceWorker';
import {rootStore} from "./stores/root.store";

ReactDOM.render(
    <Provider {...rootStore}>
        <App/>
    </Provider>,
    document.getElementById('root') as HTMLElement
);
registerServiceWorker();
