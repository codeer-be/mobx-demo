import axios from "axios";
import {IProfile} from "../../domain/models/profile";

class UsersApi {
    public async fetchUsers(): Promise<IProfile[]> {
        const response = await axios.get('http://localhost:3001/users');
        return response.data;
    }
}

export const usersApi = new UsersApi();