import axios from "axios";
import {IProfile} from "../../domain/models/profile";

class ProfileApi {
    public async fetchProfile(): Promise<IProfile> {
        const response = await axios.get('http://localhost:3001/profile');
        return response.data;
    }
}

export const profileApi = new ProfileApi();