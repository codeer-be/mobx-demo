import {inject, observer} from "mobx-react";
import * as React from 'react';
import {Drawer} from "./components/drawer/drawer.component";
import {Header} from "./components/header/header.component";
import {Users} from "./components/users/users.component";
import {AppStore} from "./stores/app/app.store";

interface IAppProps {
    appStore?: AppStore;
}

@inject(({appStore}) => ({
    appStore,
}))
@observer
export class App extends React.Component<IAppProps> {
    public componentDidMount() {
        this.props.appStore!.initApp();
    }

    public render(): React.ReactNode {
        return (
            <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
                <Header/>
                <Drawer/>
                <main className="mdl-layout__content" style={{width: 1000, margin: '50px auto 0'}}>
                    {
                        this.props.appStore!.isLoading ? (
                            <div className="mdl-typography--text-center">Loading...</div>
                        ) : (
                           <Users />
                        )
                    }
                </main>
            </div>
        );
    }
}
