import {observable} from "mobx";
import {Store} from "../store";

export class AppStore extends Store {
    @observable public isLoading: boolean = true;
    @observable public navigation = {
        menu: {
            isOpen: false
        }
    };

    public async initApp(): Promise<void> {
        this.isLoading = true;
        await this.rootStore.profileStore.fetchProfile();
        this.isLoading = false;
    }

    public toggleMenu(): void {
        this.navigation.menu.isOpen = !this.navigation.menu.isOpen;
    }
}
