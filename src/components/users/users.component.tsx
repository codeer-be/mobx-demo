import {inject, observer} from "mobx-react";
import * as React from "react";
import {UsersStore} from "../../stores/users/users.store";

interface IUsersProps {
    usersStore?: UsersStore;
}

@inject(({usersStore}) => ({
    usersStore,
}))
@observer
export class Users extends React.Component<IUsersProps> {
    public componentWillMount(): void {
        this.props.usersStore!.fetchUsers();
    }

    public render(): React.ReactNode {
        return this.props.usersStore!.isUsersLoading ? (
            <div className="mdl-typography--text-center">Users Loading...</div>
        ) : (
            <table className="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp" style={{width: '100%'}}>
                <thead>
                <tr>
                    <th className="mdl-data-table__cell--non-numeric">Username</th>
                    <th className="mdl-data-table__cell--non-numeric">First name</th>
                    <th className="mdl-data-table__cell--non-numeric">Last name</th>
                </tr>
                </thead>
                <tbody>
                {
                    this.props.usersStore!.users.map((user) => (
                        <tr key={user.username}>
                            <td className="mdl-data-table__cell--non-numeric">{user.username}</td>
                            <td className="mdl-data-table__cell--non-numeric">{user.firstName}</td>
                            <td className="mdl-data-table__cell--non-numeric">{user.lastName}</td>
                        </tr>
                    ))
                }
                </tbody>
            </table>
        );
    }
}