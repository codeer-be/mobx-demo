import {AppStore} from "./app/app.store";
import {ProfileStore} from "./profile/profile.store";
import {UsersStore} from "./users/users.store";

export class RootStore {
    public profileStore: ProfileStore;
    public usersStore: UsersStore;
    public appStore: AppStore;

    constructor() {
        this.profileStore = new ProfileStore(this);
        this.usersStore = new UsersStore(this);
        this.appStore = new AppStore(this);
    }
}

export const rootStore = new RootStore();